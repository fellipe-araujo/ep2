package vehicles;

public class Van extends Veiculo {
	
	private static final long serialVersionUID = 1L;
	
	private double rendimentoDiesel;
	private double taxaReducaoRendimentoDiesel;
	
	public Van() {
		setCombustivel("Diesel");
		setCargaMaxima(3500); //em Kg
		setVelocidadeMedia(80); //em Km/h
		setRendimentoDiesel(10); //em Km/L
		setTaxaReducaoRendimentoDiesel(0.001); //a cada Kg de carga
	}

	public double getRendimentoDiesel() {
		return rendimentoDiesel;
	}

	public void setRendimentoDiesel(double rendimentoDiesel) {
		this.rendimentoDiesel = rendimentoDiesel;
	}

	public double getTaxaReducaoRendimentoDiesel() {
		return taxaReducaoRendimentoDiesel;
	}

	public void setTaxaReducaoRendimentoDiesel(double taxaReducaoRendimentoDiesel) {
		this.taxaReducaoRendimentoDiesel = taxaReducaoRendimentoDiesel;
	}
	
	
}
