package vehicles;

public class Carro extends Veiculo {
	
	private static final long serialVersionUID = 1L;
	
	private double rendimentoGasolina;
	private double rendimentoAlcool;
	private double taxaReducaoRendimentoGasolina;
	private double taxaReducaoRendimentoAlcool;

	public Carro() {
		setCombustivel("Flex");
		setCargaMaxima(360); //em Kg
		setVelocidadeMedia(100); //em Km/h
		setRendimentoGasolina(14); //em Km/h
		setTaxaReducaoRendimentoGasolina(0.025); //em Km/L
		setRendimentoAlcool(12); //em Km/h
		setTaxaReducaoRendimentoAlcool(0.0231); //em Km/L
	}

	public double getRendimentoGasolina() {
		return rendimentoGasolina;
	}

	public void setRendimentoGasolina(double rendimentoGasolina) {
		this.rendimentoGasolina = rendimentoGasolina;
	}

	public double getRendimentoAlcool() {
		return rendimentoAlcool;
	}

	public void setRendimentoAlcool(double rendimentoAlcool) {
		this.rendimentoAlcool = rendimentoAlcool;
	}

	public double getTaxaReducaoRendimentoGasolina() {
		return taxaReducaoRendimentoGasolina;
	}

	public void setTaxaReducaoRendimentoGasolina(double taxaReducaoRendimentoGasolina) {
		this.taxaReducaoRendimentoGasolina = taxaReducaoRendimentoGasolina;
	}

	public double getTaxaReducaoRendimentoAlcool() {
		return taxaReducaoRendimentoAlcool;
	}

	public void setTaxaReducaoRendimentoAlcool(double taxaReducaoRendimentoAlcool) {
		this.taxaReducaoRendimentoAlcool = taxaReducaoRendimentoAlcool;
	}
	
	
}
