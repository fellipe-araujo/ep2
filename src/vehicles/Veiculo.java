package vehicles;

import java.io.Serializable;

public abstract class Veiculo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String combustivel;
	private double cargaMaxima;
	private double velocidadeMedia;
	
	public Veiculo() {
	}

	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public double getCargaMaxima() {
		return cargaMaxima;
	}

	public void setCargaMaxima(double cargaMaxima) {
		this.cargaMaxima = cargaMaxima;
	}

	public double getVelocidadeMedia() {
		return velocidadeMedia;
	}

	public void setVelocidadeMedia(double velocidadeMedia) {
		this.velocidadeMedia = velocidadeMedia;
	}
	
	public double calculaReducaoRendimento(double cargaProposta, double taxaReducaoRendimento) {
		return cargaProposta * taxaReducaoRendimento;
	}
}
