package vehicles;

public class Carreta extends Veiculo {
	
	private static final long serialVersionUID = 1L;
	
	private double rendimentoDiesel;
	private double taxaReducaoRendimentoDiesel;
	
	public Carreta() {
		setCombustivel("Diesel");
		setCargaMaxima(30000); //em Kg
		setVelocidadeMedia(60); //em Km/h
		setRendimentoDiesel(8); //em Km/L
		setTaxaReducaoRendimentoDiesel(0.0002); //a cada Kg de carga
	}

	public double getRendimentoDiesel() {
		return rendimentoDiesel;
	}

	public void setRendimentoDiesel(double rendimentoDiesel) {
		this.rendimentoDiesel = rendimentoDiesel;
	}

	public double getTaxaReducaoRendimentoDiesel() {
		return taxaReducaoRendimentoDiesel;
	}

	public void setTaxaReducaoRendimentoDiesel(double taxaReducaoRendimentoDiesel) {
		this.taxaReducaoRendimentoDiesel = taxaReducaoRendimentoDiesel;
	}
	
	
}
