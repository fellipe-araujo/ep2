package persistence;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import application.Sistema;

public class RestaurarObjeto {
	
	public Sistema restaurarSistema(Sistema sistemaRestaurado){
		
		try {
			FileInputStream restFile = new FileInputStream("../ep2/src/persistence/Sistema.dat");
			ObjectInputStream stream = new ObjectInputStream(restFile);
			
			sistemaRestaurado = (Sistema) stream.readObject();
			stream.close();
			restFile.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return sistemaRestaurado;
	}
}
