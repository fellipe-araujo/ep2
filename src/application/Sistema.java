package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import vehicles.Veiculo;

public class Sistema implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<Veiculo> listaCarretaDisponivel;
	private List<Veiculo> listaCarretaEmRota;
	private List<Veiculo> listaVanDisponivel;
	private List<Veiculo> listaVanEmRota;
	private List<Veiculo> listaCarroDisponivel;
	private List<Veiculo> listaCarroEmRota;
	private List<Veiculo> listaMotoDisponivel;
	private List<Veiculo> listaMotoEmRota;
	private double lucro;
	private double precoDiesel;
	private double precoGasolina;
	private double precoAlcool;
	private String tipoVeiculoMaisRapido;
	private double tempoMaximoVeiculoMaisRapido;
	private double valorFreteVeiculoMaisRapido;
	private String tipoVeiculoMaisBarato;
	private double tempoMaximoVeiculoMaisBarato;
	private double valorFreteVeiculoMaisBarato;
	private String tipoVeiculoMelhorCustoBeneficio;
	private double tempoMaximoVeiculoMelhorCustoBeneficio;
	private double valorFreteVeiculoMelhorCustoBeneficio;
	private boolean veiculoMaisBarato;
	private boolean veiculoMaisRapido;
	private boolean veiculoMelhorCustoBeneficio;
	
	public Sistema() {
		listaCarretaDisponivel = new ArrayList<>();
		listaCarretaEmRota = new ArrayList<>();
		listaVanDisponivel = new ArrayList<>();
		listaVanEmRota = new ArrayList<>();
		listaCarroDisponivel = new ArrayList<>();
		listaCarroEmRota = new ArrayList<>();
		listaMotoDisponivel = new ArrayList<>();
		listaMotoEmRota = new ArrayList<>();
		lucro = 20.0;
		precoDiesel = 3.869;
		precoGasolina = 4.449;
		precoAlcool = 3.499;
		tipoVeiculoMaisRapido = "nenhum";
		tempoMaximoVeiculoMaisRapido = 0.0;
		valorFreteVeiculoMaisRapido = 0.0;
		tipoVeiculoMaisBarato = "nenhum";
		tempoMaximoVeiculoMaisBarato = 0.0;
		valorFreteVeiculoMaisBarato = 0.0;
		tipoVeiculoMelhorCustoBeneficio = "nenhum";
		tempoMaximoVeiculoMelhorCustoBeneficio = 0.0;
		valorFreteVeiculoMelhorCustoBeneficio = 0.0;
		veiculoMaisBarato = false;
		veiculoMaisRapido = false;
		veiculoMelhorCustoBeneficio = false;
	}

	public List<Veiculo> getListaCarretaDisponivel() {
		return listaCarretaDisponivel;
	}

	public void setListaCarretaDisponivel(List<Veiculo> listaCarretaDisponivel) {
		this.listaCarretaDisponivel = listaCarretaDisponivel;
	}

	public List<Veiculo> getListaCarretaEmRota() {
		return listaCarretaEmRota;
	}

	public void setListaCarretaEmRota(List<Veiculo> listaCarretaEmRota) {
		this.listaCarretaEmRota = listaCarretaEmRota;
	}

	public List<Veiculo> getListaVanDisponivel() {
		return listaVanDisponivel;
	}

	public void setListaVanDisponivel(List<Veiculo> listaVanDisponivel) {
		this.listaVanDisponivel = listaVanDisponivel;
	}

	public List<Veiculo> getListaVanEmRota() {
		return listaVanEmRota;
	}

	public void setListaVanEmRota(List<Veiculo> listaVanEmRota) {
		this.listaVanEmRota = listaVanEmRota;
	}

	public List<Veiculo> getListaCarroDisponivel() {
		return listaCarroDisponivel;
	}

	public void setListaCarroDisponivel(List<Veiculo> listaCarroDisponivel) {
		this.listaCarroDisponivel = listaCarroDisponivel;
	}

	public List<Veiculo> getListaCarroEmRota() {
		return listaCarroEmRota;
	}

	public void setListaCarroEmRota(List<Veiculo> listaCarroEmRota) {
		this.listaCarroEmRota = listaCarroEmRota;
	}

	public List<Veiculo> getListaMotoDisponivel() {
		return listaMotoDisponivel;
	}

	public void setListaMotoDisponivel(List<Veiculo> listaMotoDisponivel) {
		this.listaMotoDisponivel = listaMotoDisponivel;
	}

	public List<Veiculo> getListaMotoEmRota() {
		return listaMotoEmRota;
	}

	public void setListaMotoEmRota(List<Veiculo> listaMotoEmRota) {
		this.listaMotoEmRota = listaMotoEmRota;
	}

	public double getLucro() {
		return lucro;
	}

	public void setLucro(double lucro) {
		this.lucro = lucro;
	}

	public double getPrecoDiesel() {
		return precoDiesel;
	}

	public void setPrecoDiesel(double precoDiesel) {
		this.precoDiesel = precoDiesel;
	}

	public double getPrecoGasolina() {
		return precoGasolina;
	}

	public void setPrecoGasolina(double precoGasolina) {
		this.precoGasolina = precoGasolina;
	}

	public double getPrecoAlcool() {
		return precoAlcool;
	}

	public void setPrecoAlcool(double precoAlcool) {
		this.precoAlcool = precoAlcool;
	}
	
	public String getTipoVeiculoMaisRapido() {
		return tipoVeiculoMaisRapido;
	}

	public void setTipoVeiculoMaisRapido(String tipoVeiculoMaisRapido) {
		this.tipoVeiculoMaisRapido = tipoVeiculoMaisRapido;
	}

	public double getTempoMaximoVeiculoMaisRapido() {
		return tempoMaximoVeiculoMaisRapido;
	}

	public void setTempoMaximoVeiculoMaisRapido(double tempoMaximoVeiculoMaisRapido) {
		this.tempoMaximoVeiculoMaisRapido = tempoMaximoVeiculoMaisRapido;
	}

	public double getValorFreteVeiculoMaisRapido() {
		return valorFreteVeiculoMaisRapido;
	}

	public void setValorFreteVeiculoMaisRapido(double valorFreteVeiculoMaisRapido) {
		this.valorFreteVeiculoMaisRapido = valorFreteVeiculoMaisRapido;
	}

	public String getTipoVeiculoMaisBarato() {
		return tipoVeiculoMaisBarato;
	}

	public void setTipoVeiculoMaisBarato(String tipoVeiculoMaisBarato) {
		this.tipoVeiculoMaisBarato = tipoVeiculoMaisBarato;
	}

	public double getTempoMaximoVeiculoMaisBarato() {
		return tempoMaximoVeiculoMaisBarato;
	}

	public void setTempoMaximoVeiculoMaisBarato(double tempoMaximoVeiculoMaisBarato) {
		this.tempoMaximoVeiculoMaisBarato = tempoMaximoVeiculoMaisBarato;
	}

	public double getValorFreteVeiculoMaisBarato() {
		return valorFreteVeiculoMaisBarato;
	}

	public void setValorFreteVeiculoMaisBarato(double valorFreteVeiculoMaisBarato) {
		this.valorFreteVeiculoMaisBarato = valorFreteVeiculoMaisBarato;
	}

	public String getTipoVeiculoMelhorCustoBeneficio() {
		return tipoVeiculoMelhorCustoBeneficio;
	}

	public void setTipoVeiculoMelhorCustoBeneficio(String tipoVeiculoMelhorCustoBeneficio) {
		this.tipoVeiculoMelhorCustoBeneficio = tipoVeiculoMelhorCustoBeneficio;
	}

	public double getTempoMaximoVeiculoMelhorCustoBeneficio() {
		return tempoMaximoVeiculoMelhorCustoBeneficio;
	}

	public void setTempoMaximoVeiculoMelhorCustoBeneficio(double tempoMaximoVeiculoMelhorCustoBeneficio) {
		this.tempoMaximoVeiculoMelhorCustoBeneficio = tempoMaximoVeiculoMelhorCustoBeneficio;
	}

	public double getValorFreteVeiculoMelhorCustoBeneficio() {
		return valorFreteVeiculoMelhorCustoBeneficio;
	}

	public void setValorFreteVeiculoMelhorCustoBeneficio(double valorFreteVeiculoMelhorCustoBeneficio) {
		this.valorFreteVeiculoMelhorCustoBeneficio = valorFreteVeiculoMelhorCustoBeneficio;
	}

	public boolean getVeiculoMaisBarato() {
		return veiculoMaisBarato;
	}

	public void setVeiculoMaisBarato(boolean veiculoMaisBarato) {
		this.veiculoMaisBarato = veiculoMaisBarato;
	}

	public boolean getVeiculoMaisRapido() {
		return veiculoMaisRapido;
	}

	public void setVeiculoMaisRapido(boolean veiculoMaisRapido) {
		this.veiculoMaisRapido = veiculoMaisRapido;
	}

	public boolean getVeiculoMelhorCustoBeneficio() {
		return veiculoMelhorCustoBeneficio;
	}

	public void setVeiculoMelhorCustoBeneficio(boolean veiculoMelhorCustoBeneficio) {
		this.veiculoMelhorCustoBeneficio = veiculoMelhorCustoBeneficio;
	}

	public double calculaTempoMaximo(double distancia, double velocidadeMedia, String tipoVeiculo) {
		return distancia / velocidadeMedia;
	}
	
	//tipoCombustivel = 1  ---  Gasolina ou Diesel
	public double calculaCusto(double distancia, double rendimento, String tipoVeiculo, int tipoCombustivel) {
		double custo = 0.0;
		if(tipoVeiculo == "carreta" || tipoVeiculo == "van") {
			custo = (distancia / rendimento) * precoDiesel;
		}
		else if(tipoVeiculo == "carro" || tipoVeiculo == "moto") {
			if(tipoCombustivel == 1) {
				custo = (distancia / rendimento) * precoGasolina;
			}
			else {
				custo = (distancia / rendimento) * precoAlcool;
			}
		}
		return custo;
	}
	
	public double calculaCustoBeneficio(double custo, double tempo, String tipoVeiculo) {
		return custo / tempo;
	}
}
