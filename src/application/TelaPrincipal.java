package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import persistence.RestaurarObjeto;
import persistence.SalvarObjeto;
import vehicles.Carreta;
import vehicles.Carro;
import vehicles.Moto;
import vehicles.Van;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;

public class TelaPrincipal extends JFrame {

	private static final long serialVersionUID = 1191420329748369622L;
	
	private JPanel contentPane;
	private JTextField textFieldCadastrarCarreta;
	private JTextField textFieldCadastrarVan;
	private JTextField textFieldCadastrarCarro;
	private JTextField textFieldCadastrarMoto;
	private JTextField textFieldDescadastrarCarreta;
	private JTextField textFieldDescadastrarVan;
	private JTextField textFieldDescadastrarCarro;
	private JTextField textFieldDescadastrarMoto;
	private JTextField textFieldRetornarCarreta;
	private JTextField textFieldRetornarVan;
	private JTextField textFieldRetornarCarro;
	private JTextField textFieldRetornarMoto;
	private JTextField textFieldTaxaLucro;
	private JTextField textFieldPrecoAlcool;
	private JTextField textFieldPrecoGasolina;
	private JTextField textFieldPrecoDiesel;
	private JTextField textFieldPesoDaCarga;
	private JTextField textFieldDistancia;
	private JTextField textFieldTempoMaximo;
	
	private JLabel lblQtdTotalVeiculos;
	private JLabel lblQtdCarretaDisponivel;
	private JLabel lblQtdVanDisponivel;
	private JLabel lblQtdCarroDisponivel;
	private JLabel lblQtdMotoDisponivel;
	private JLabel lblQtdCarretaRota;
	private JLabel lblQtdVanRota;
	private JLabel lblQtdCarroRota;
	private JLabel lblQtdMotoRota;
	
	private Sistema sistema = new Sistema();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		//sistema = new Sistema();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1028, 783);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1012, 744);
		contentPane.add(tabbedPane);
		
		JPanel AbaInicio = new JPanel();
		tabbedPane.addTab("In\u00EDcio", null, AbaInicio, null);
		AbaInicio.setLayout(null);
		
		JLabel lblTransportadoraOofga = new JLabel("Transportadora OOFGA");
		lblTransportadoraOofga.setFont(new Font("Algerian", Font.PLAIN, 38));
		lblTransportadoraOofga.setHorizontalAlignment(SwingConstants.CENTER);
		lblTransportadoraOofga.setBounds(174, 45, 606, 143);
		AbaInicio.add(lblTransportadoraOofga);
		
		JLabel lblSenhorUsurioLembrese = new JLabel("Senhor usu\u00E1rio, lembre-se de salvar ou carregar o sistema antes de fechar o programa ou abrir toda vez que usar.");
		lblSenhorUsurioLembrese.setForeground(Color.RED);
		lblSenhorUsurioLembrese.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblSenhorUsurioLembrese.setBounds(10, 648, 886, 39);
		AbaInicio.add(lblSenhorUsurioLembrese);
		
		JPanel AbaVeiculos = new JPanel();
		AbaVeiculos.setToolTipText("");
		tabbedPane.addTab("Ve\u00EDculos", null, AbaVeiculos, null);
		AbaVeiculos.setLayout(null);
		
		JLabel lblCadastrarVeiculos = new JLabel("Insira a quantidade de ve\u00EDculos a ser cadastrado na empresa:");
		lblCadastrarVeiculos.setBounds(10, 11, 386, 21);
		AbaVeiculos.add(lblCadastrarVeiculos);
		
		JLabel lblCadastrarCarreta = new JLabel("Carreta:");
		lblCadastrarCarreta.setBounds(20, 43, 57, 21);
		AbaVeiculos.add(lblCadastrarCarreta);
		
		JLabel lblCadastrarVan = new JLabel("Van:");
		lblCadastrarVan.setBounds(20, 75, 57, 21);
		AbaVeiculos.add(lblCadastrarVan);
		
		JLabel lblCadastrarCarro = new JLabel("Carro:");
		lblCadastrarCarro.setBounds(20, 107, 57, 21);
		AbaVeiculos.add(lblCadastrarCarro);
		
		JLabel lblCadastrarMoto = new JLabel("Moto:");
		lblCadastrarMoto.setBounds(20, 139, 57, 21);
		AbaVeiculos.add(lblCadastrarMoto);
		
		textFieldCadastrarCarreta = new JTextField();
		textFieldCadastrarCarreta.setText("0");
		textFieldCadastrarCarreta.setBounds(99, 43, 70, 20);
		AbaVeiculos.add(textFieldCadastrarCarreta);
		textFieldCadastrarCarreta.setColumns(10);
		
		textFieldCadastrarVan = new JTextField();
		textFieldCadastrarVan.setText("0");
		textFieldCadastrarVan.setBounds(99, 75, 70, 20);
		AbaVeiculos.add(textFieldCadastrarVan);
		textFieldCadastrarVan.setColumns(10);
		
		textFieldCadastrarCarro = new JTextField();
		textFieldCadastrarCarro.setText("0");
		textFieldCadastrarCarro.setBounds(99, 107, 70, 20);
		AbaVeiculos.add(textFieldCadastrarCarro);
		textFieldCadastrarCarro.setColumns(10);
		
		textFieldCadastrarMoto = new JTextField();
		textFieldCadastrarMoto.setText("0");
		textFieldCadastrarMoto.setBounds(99, 139, 70, 20);
		AbaVeiculos.add(textFieldCadastrarMoto);
		textFieldCadastrarMoto.setColumns(10);
		
		JButton btnCadastrarCarreta = new JButton("Cadastrar");
		btnCadastrarCarreta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int carretasParaCadastrar, carretasJaCadastradas, carretasDisponiveisTotal;
				int veiculosTotais, veiculosJaExistentes;
				if(comando.equals("Cadastrar")) {
					carretasJaCadastradas = Integer.parseInt(lblQtdCarretaDisponivel.getText());
					carretasParaCadastrar = Integer.parseInt(textFieldCadastrarCarreta.getText());
					carretasDisponiveisTotal = carretasJaCadastradas + carretasParaCadastrar;
					lblQtdCarretaDisponivel.setText(String.valueOf(carretasDisponiveisTotal));
					veiculosJaExistentes = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais = veiculosJaExistentes + carretasParaCadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					for(int i = 0; i < carretasParaCadastrar; i++) {
						Carreta carreta = new Carreta();
						sistema.getListaCarretaDisponivel().add(carreta);
					}
				}
			}
		});
		btnCadastrarCarreta.setBounds(179, 42, 118, 23);
		AbaVeiculos.add(btnCadastrarCarreta);
		
		JButton btnCadastrarVan = new JButton("Cadastrar");
		btnCadastrarVan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int vansParaCadastrar, vansJaCadastradas, vansDisponiveisTotal;
				int veiculosTotais, veiculosJaExistentes;
				if(comando.equals("Cadastrar")) {
					vansJaCadastradas = Integer.parseInt(lblQtdVanDisponivel.getText());
					vansParaCadastrar = Integer.parseInt(textFieldCadastrarVan.getText());
					vansDisponiveisTotal = vansJaCadastradas + vansParaCadastrar;
					lblQtdVanDisponivel.setText(String.valueOf(vansDisponiveisTotal));
					veiculosJaExistentes = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais = veiculosJaExistentes + vansParaCadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					for(int i = 0; i < vansParaCadastrar; i++) {
						Van van = new Van();
						sistema.getListaVanDisponivel().add(van);
					}
				}
			}
		});
		btnCadastrarVan.setBounds(179, 74, 118, 23);
		AbaVeiculos.add(btnCadastrarVan);
		
		JButton btnCadastrarCarro = new JButton("Cadastrar");
		btnCadastrarCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int carrosParaCadastrar, carrosJaCadastradas, carrosDisponiveisTotal;
				int veiculosTotais, veiculosJaExistentes;
				if(comando.equals("Cadastrar")) {
					carrosJaCadastradas = Integer.parseInt(lblQtdCarroDisponivel.getText());
					carrosParaCadastrar = Integer.parseInt(textFieldCadastrarCarro.getText());
					carrosDisponiveisTotal = carrosJaCadastradas + carrosParaCadastrar;
					lblQtdCarroDisponivel.setText(String.valueOf(carrosDisponiveisTotal));
					veiculosJaExistentes = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais = veiculosJaExistentes + carrosParaCadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					for(int i = 0; i < carrosParaCadastrar; i++) {
						Carro carro = new Carro();
						sistema.getListaCarroDisponivel().add(carro);
					}
				}
			}
		});
		btnCadastrarCarro.setBounds(179, 106, 118, 23);
		AbaVeiculos.add(btnCadastrarCarro);
		
		JButton btnCadastrarMoto = new JButton("Cadastrar");
		btnCadastrarMoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int motosParaCadastrar, motosJaCadastradas, motosDisponiveisTotal;
				int veiculosTotais, veiculosJaExistentes;
				if(comando.equals("Cadastrar")) {
					motosJaCadastradas = Integer.parseInt(lblQtdMotoDisponivel.getText());
					motosParaCadastrar = Integer.parseInt(textFieldCadastrarMoto.getText());
					motosDisponiveisTotal = motosJaCadastradas + motosParaCadastrar;
					lblQtdMotoDisponivel.setText(String.valueOf(motosDisponiveisTotal));
					veiculosJaExistentes = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais = veiculosJaExistentes + motosParaCadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					for(int i = 0; i < motosParaCadastrar; i++) {
						Moto moto = new Moto();
						sistema.getListaMotoDisponivel().add(moto);
					}
				}
			}
		});
		btnCadastrarMoto.setBounds(179, 138, 118, 23);
		AbaVeiculos.add(btnCadastrarMoto);
		
		
		
		JLabel lblDescadastrarVeiculos = new JLabel("Insira a quantidade de ve\u00EDculos a ser descadastrado da empresa:");
		lblDescadastrarVeiculos.setBounds(10, 243, 386, 21);
		AbaVeiculos.add(lblDescadastrarVeiculos);
		
		JLabel lblDescadastrarCarreta = new JLabel("Carreta:");
		lblDescadastrarCarreta.setBounds(20, 279, 57, 21);
		AbaVeiculos.add(lblDescadastrarCarreta);
		
		JLabel lblDescadastrarVan = new JLabel("Van:");
		lblDescadastrarVan.setBounds(20, 311, 57, 21);
		AbaVeiculos.add(lblDescadastrarVan);
		
		JLabel lblDescadastrarCarro = new JLabel("Carro:");
		lblDescadastrarCarro.setBounds(20, 343, 57, 21);
		AbaVeiculos.add(lblDescadastrarCarro);
		
		JLabel lblDescadastrarMoto = new JLabel("Moto:");
		lblDescadastrarMoto.setBounds(20, 375, 57, 21);
		AbaVeiculos.add(lblDescadastrarMoto);
		
		textFieldDescadastrarCarreta = new JTextField();
		textFieldDescadastrarCarreta.setText("0");
		textFieldDescadastrarCarreta.setColumns(10);
		textFieldDescadastrarCarreta.setBounds(99, 279, 70, 20);
		AbaVeiculos.add(textFieldDescadastrarCarreta);
		
		textFieldDescadastrarVan = new JTextField();
		textFieldDescadastrarVan.setText("0");
		textFieldDescadastrarVan.setColumns(10);
		textFieldDescadastrarVan.setBounds(99, 311, 70, 20);
		AbaVeiculos.add(textFieldDescadastrarVan);
		
		textFieldDescadastrarCarro = new JTextField();
		textFieldDescadastrarCarro.setText("0");
		textFieldDescadastrarCarro.setColumns(10);
		textFieldDescadastrarCarro.setBounds(99, 343, 70, 20);
		AbaVeiculos.add(textFieldDescadastrarCarro);
		
		textFieldDescadastrarMoto = new JTextField();
		textFieldDescadastrarMoto.setText("0");
		textFieldDescadastrarMoto.setColumns(10);
		textFieldDescadastrarMoto.setBounds(99, 375, 70, 20);
		AbaVeiculos.add(textFieldDescadastrarMoto);
		
		JButton btnDescadastrarCarreta = new JButton("Descadastrar");
		btnDescadastrarCarreta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int veiculosTotais;
				int carretasParaDescadastrar = Integer.parseInt(textFieldDescadastrarCarreta.getText());
				int carretasDisponiveis = Integer.parseInt(lblQtdCarretaDisponivel.getText());
				if(comando.equals("Descadastrar") && carretasParaDescadastrar <= carretasDisponiveis) {
					veiculosTotais = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais -= carretasParaDescadastrar;
					carretasDisponiveis -= carretasParaDescadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					lblQtdCarretaDisponivel.setText(String.valueOf(carretasDisponiveis));
					int i = sistema.getListaCarretaDisponivel().size() - 1;
					int j = sistema.getListaCarretaDisponivel().size() - (carretasParaDescadastrar + 1);
					for(int m = i; m > j; m--) {
						sistema.getListaCarretaDisponivel().remove(m);
					}
				}
			}
		});
		btnDescadastrarCarreta.setBounds(179, 278, 118, 23);
		AbaVeiculos.add(btnDescadastrarCarreta);
		
		JButton btnDescadastrarVan = new JButton("Descadastrar");
		btnDescadastrarVan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int veiculosTotais;
				int vansParaDescadastrar = Integer.parseInt(textFieldDescadastrarVan.getText());
				int vansDisponiveis = Integer.parseInt(lblQtdVanDisponivel.getText());
				if(comando.equals("Descadastrar") && vansParaDescadastrar <= vansDisponiveis) {
					veiculosTotais = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais -= vansParaDescadastrar;
					vansDisponiveis -= vansParaDescadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					lblQtdVanDisponivel.setText(String.valueOf(vansDisponiveis));
					int i = sistema.getListaVanDisponivel().size() - 1;
					int j = sistema.getListaVanDisponivel().size() - (vansParaDescadastrar + 1);
					for(int m = i; m > j; m--) {
						sistema.getListaVanDisponivel().remove(m);
					}
				}
			}
		});
		btnDescadastrarVan.setBounds(179, 310, 118, 23);
		AbaVeiculos.add(btnDescadastrarVan);
		
		JButton btnDescadastrarCarro = new JButton("Descadastrar");
		btnDescadastrarCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int veiculosTotais;
				int carrosParaDescadastrar = Integer.parseInt(textFieldDescadastrarCarro.getText());
				int carrosDisponiveis = Integer.parseInt(lblQtdCarroDisponivel.getText());
				if(comando.equals("Descadastrar") && carrosParaDescadastrar <= carrosDisponiveis) {
					veiculosTotais = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais -= carrosParaDescadastrar;
					carrosDisponiveis -= carrosParaDescadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					lblQtdCarroDisponivel.setText(String.valueOf(carrosDisponiveis));
					int i = sistema.getListaCarroDisponivel().size() - 1;
					int j = sistema.getListaCarroDisponivel().size() - (carrosParaDescadastrar + 1);
					for(int m = i; m > j; m--) {
						sistema.getListaCarroDisponivel().remove(m);
					}
				}
			}
		});
		btnDescadastrarCarro.setBounds(179, 342, 118, 23);
		AbaVeiculos.add(btnDescadastrarCarro);
		
		JButton btnDescadastrarMoto = new JButton("Descadastrar");
		btnDescadastrarMoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int veiculosTotais;
				int motosParaDescadastrar = Integer.parseInt(textFieldDescadastrarMoto.getText());
				int motosDisponiveis = Integer.parseInt(lblQtdMotoDisponivel.getText());
				if(comando.equals("Descadastrar") && motosParaDescadastrar <= motosDisponiveis) {
					veiculosTotais = Integer.parseInt(lblQtdTotalVeiculos.getText());
					veiculosTotais -= motosParaDescadastrar;
					motosDisponiveis -= motosParaDescadastrar;
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					lblQtdMotoDisponivel.setText(String.valueOf(motosDisponiveis));
					int i = sistema.getListaMotoDisponivel().size() - 1;
					int j = sistema.getListaMotoDisponivel().size() - (motosParaDescadastrar + 1);
					for(int m = i; m > j; m--) {
						sistema.getListaMotoDisponivel().remove(m);
					}
				}
			}
		});
		btnDescadastrarMoto.setBounds(179, 374, 118, 23);
		AbaVeiculos.add(btnDescadastrarMoto);
		
		JLabel lblRetornarVeiculos = new JLabel("Insira a quantidade de ve\u00EDculos que retornou da entrega:");
		lblRetornarVeiculos.setBounds(10, 472, 386, 21);
		AbaVeiculos.add(lblRetornarVeiculos);
		
		JLabel lblRetornarCarreta = new JLabel("Carreta:");
		lblRetornarCarreta.setBounds(20, 504, 57, 21);
		AbaVeiculos.add(lblRetornarCarreta);
		
		JLabel lblRetornarVan = new JLabel("Van:");
		lblRetornarVan.setBounds(20, 536, 57, 21);
		AbaVeiculos.add(lblRetornarVan);
		
		JLabel lblRetornarCarro = new JLabel("Carro:");
		lblRetornarCarro.setBounds(20, 568, 57, 21);
		AbaVeiculos.add(lblRetornarCarro);
		
		JLabel lblRetornarMoto = new JLabel("Moto:");
		lblRetornarMoto.setBounds(20, 600, 57, 21);
		AbaVeiculos.add(lblRetornarMoto);
		
		textFieldRetornarCarreta = new JTextField();
		textFieldRetornarCarreta.setText("0");
		textFieldRetornarCarreta.setColumns(10);
		textFieldRetornarCarreta.setBounds(99, 504, 70, 20);
		AbaVeiculos.add(textFieldRetornarCarreta);
		
		textFieldRetornarVan = new JTextField();
		textFieldRetornarVan.setText("0");
		textFieldRetornarVan.setColumns(10);
		textFieldRetornarVan.setBounds(99, 536, 70, 20);
		AbaVeiculos.add(textFieldRetornarVan);
		
		textFieldRetornarCarro = new JTextField();
		textFieldRetornarCarro.setText("0");
		textFieldRetornarCarro.setColumns(10);
		textFieldRetornarCarro.setBounds(99, 568, 70, 20);
		AbaVeiculos.add(textFieldRetornarCarro);
		
		textFieldRetornarMoto = new JTextField();
		textFieldRetornarMoto.setText("0");
		textFieldRetornarMoto.setColumns(10);
		textFieldRetornarMoto.setBounds(99, 600, 70, 20);
		AbaVeiculos.add(textFieldRetornarMoto);
		
		JButton btnRetornarCarreta = new JButton("Retornar");
		btnRetornarCarreta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int carretasParaRetornar = Integer.parseInt(textFieldRetornarCarreta.getText());
				int carretasDisponiveis = Integer.parseInt(lblQtdCarretaDisponivel.getText());
				int carretasEmRota = Integer.parseInt(lblQtdCarretaRota.getText());
				if(comando.equals("Retornar") && carretasParaRetornar <= carretasEmRota) {
					int i = sistema.getListaCarretaEmRota().size() - 1;
					int j = sistema.getListaCarretaEmRota().size() - (carretasParaRetornar + 1);
					for(int m = i; m > j; m--) {
						Carreta carreta = new Carreta();
						carreta = (Carreta) sistema.getListaCarretaEmRota().get(m);
						sistema.getListaCarretaDisponivel().add(carreta);
						sistema.getListaCarretaEmRota().remove(m);
					}
					carretasDisponiveis += carretasParaRetornar;
					carretasEmRota -= carretasParaRetornar;
					lblQtdCarretaDisponivel.setText(String.valueOf(carretasDisponiveis));
					lblQtdCarretaRota.setText(String.valueOf(carretasEmRota));
				}
			}
		});
		btnRetornarCarreta.setBounds(179, 503, 118, 23);
		AbaVeiculos.add(btnRetornarCarreta);
		
		JButton btnRetornarVan = new JButton("Retornar");
		btnRetornarVan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int vansParaRetornar = Integer.parseInt(textFieldRetornarVan.getText());
				int vansDisponiveis = Integer.parseInt(lblQtdVanDisponivel.getText());
				int vansEmRota = Integer.parseInt(lblQtdVanRota.getText());
				if(comando.equals("Retornar") && vansParaRetornar <= vansEmRota) {
					int i = sistema.getListaVanEmRota().size() - 1;
					int j = sistema.getListaVanEmRota().size() - (vansParaRetornar + 1);
					for(int m = i; m > j; m--) {
						Van van = new Van();
						van = (Van) sistema.getListaVanEmRota().get(m);
						sistema.getListaVanDisponivel().add(van);
						sistema.getListaVanEmRota().remove(m);
					}
					vansDisponiveis += vansParaRetornar;
					vansEmRota -= vansParaRetornar;
					lblQtdVanDisponivel.setText(String.valueOf(vansDisponiveis));
					lblQtdVanRota.setText(String.valueOf(vansEmRota));
				}
			}
		});
		btnRetornarVan.setBounds(179, 535, 118, 23);
		AbaVeiculos.add(btnRetornarVan);
		
		JButton btnRetornarCarro = new JButton("Retornar");
		btnRetornarCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int carrosParaRetornar = Integer.parseInt(textFieldRetornarCarro.getText());
				int carrosDisponiveis = Integer.parseInt(lblQtdCarroDisponivel.getText());
				int carrosEmRota = Integer.parseInt(lblQtdCarroRota.getText());
				if(comando.equals("Retornar") && carrosParaRetornar <= carrosEmRota) {
					int i = sistema.getListaCarroEmRota().size() - 1;
					int j = sistema.getListaCarroEmRota().size() - (carrosParaRetornar + 1);
					for(int m = i; m > j; m--) {
						Carro carro = new Carro();
						carro = (Carro) sistema.getListaCarroEmRota().get(m);
						sistema.getListaCarroDisponivel().add(carro);
						sistema.getListaCarroEmRota().remove(m);
					}
					carrosDisponiveis += carrosParaRetornar;
					carrosEmRota -= carrosParaRetornar;
					lblQtdCarroDisponivel.setText(String.valueOf(carrosDisponiveis));
					lblQtdCarroRota.setText(String.valueOf(carrosEmRota));
				}
			}
		});
		btnRetornarCarro.setBounds(179, 567, 118, 23);
		AbaVeiculos.add(btnRetornarCarro);
		
		JButton btnRetornarMoto = new JButton("Retornar");
		btnRetornarMoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				int motosParaRetornar = Integer.parseInt(textFieldRetornarMoto.getText());
				int motosDisponiveis = Integer.parseInt(lblQtdMotoDisponivel.getText());
				int motosEmRota = Integer.parseInt(lblQtdMotoRota.getText());
				if(comando.equals("Retornar") && motosParaRetornar <= motosEmRota) {
					int i = sistema.getListaMotoEmRota().size() - 1;
					int j = sistema.getListaMotoEmRota().size() - (motosParaRetornar + 1);
					for(int m = i; m > j; m--) {
						Moto moto = new Moto();
						moto = (Moto) sistema.getListaMotoEmRota().get(m);
						sistema.getListaMotoDisponivel().add(moto);
						sistema.getListaMotoEmRota().remove(m);
					}
					motosDisponiveis += motosParaRetornar;
					motosEmRota -= motosParaRetornar;
					lblQtdMotoDisponivel.setText(String.valueOf(motosDisponiveis));
					lblQtdMotoRota.setText(String.valueOf(motosEmRota));
				}
			}
		});
		btnRetornarMoto.setBounds(179, 599, 118, 23);
		AbaVeiculos.add(btnRetornarMoto);
		
		JLabel lblTextoTotalVeiculos = new JLabel("Total de ve\u00EDculos da empresa:");
		lblTextoTotalVeiculos.setBounds(481, 14, 195, 18);
		AbaVeiculos.add(lblTextoTotalVeiculos);
		
		lblQtdTotalVeiculos = new JLabel("0");
		lblQtdTotalVeiculos.setForeground(Color.BLUE);
		lblQtdTotalVeiculos.setBounds(686, 13, 32, 21);
		AbaVeiculos.add(lblQtdTotalVeiculos);
		
		JLabel lblVeiculosDisponveis = new JLabel("Ve\u00EDculos Dispon\u00EDveis:");
		lblVeiculosDisponveis.setBounds(481, 64, 163, 21);
		AbaVeiculos.add(lblVeiculosDisponveis);
		
		JLabel lblTextoCarretaDisponivel = new JLabel("Carreta:");
		lblTextoCarretaDisponivel.setBounds(491, 96, 48, 21);
		AbaVeiculos.add(lblTextoCarretaDisponivel);
		
		lblQtdCarretaDisponivel = new JLabel("0");
		lblQtdCarretaDisponivel.setForeground(Color.GREEN);
		lblQtdCarretaDisponivel.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdCarretaDisponivel.setBounds(491, 128, 42, 21);
		AbaVeiculos.add(lblQtdCarretaDisponivel);
		
		JLabel lblTextoVanDisponveis = new JLabel("Van:");
		lblTextoVanDisponveis.setBounds(559, 96, 48, 21);
		AbaVeiculos.add(lblTextoVanDisponveis);
		
		lblQtdVanDisponivel = new JLabel("0");
		lblQtdVanDisponivel.setForeground(Color.GREEN);
		lblQtdVanDisponivel.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdVanDisponivel.setBounds(543, 128, 56, 21);
		AbaVeiculos.add(lblQtdVanDisponivel);
		
		JLabel lblTextoCarroDisponveis = new JLabel("Carro:");
		lblTextoCarroDisponveis.setBounds(628, 96, 48, 21);
		AbaVeiculos.add(lblTextoCarroDisponveis);
		
		lblQtdCarroDisponivel = new JLabel("0");
		lblQtdCarroDisponivel.setForeground(Color.GREEN);
		lblQtdCarroDisponivel.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdCarroDisponivel.setBounds(628, 128, 32, 21);
		AbaVeiculos.add(lblQtdCarroDisponivel);
		
		JLabel lblTextoMotoDisponveis = new JLabel("Moto:");
		lblTextoMotoDisponveis.setBounds(699, 96, 48, 21);
		AbaVeiculos.add(lblTextoMotoDisponveis);
		
		lblQtdMotoDisponivel = new JLabel("0");
		lblQtdMotoDisponivel.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdMotoDisponivel.setForeground(Color.GREEN);
		lblQtdMotoDisponivel.setBounds(689, 128, 42, 21);
		AbaVeiculos.add(lblQtdMotoDisponivel);
		
		JLabel lblVeiculosEmRota = new JLabel("Ve\u00EDculos em Rota:");
		lblVeiculosEmRota.setBounds(481, 193, 163, 21);
		AbaVeiculos.add(lblVeiculosEmRota);
		
		JLabel lblTextoCarretaRota = new JLabel("Carreta:");
		lblTextoCarretaRota.setBounds(491, 225, 48, 21);
		AbaVeiculos.add(lblTextoCarretaRota);
		
		JLabel lblTextoVanRota = new JLabel("Van:");
		lblTextoVanRota.setBounds(559, 225, 48, 21);
		AbaVeiculos.add(lblTextoVanRota);
		
		JLabel lblTextoCarroRota = new JLabel("Carro:");
		lblTextoCarroRota.setBounds(628, 225, 48, 21);
		AbaVeiculos.add(lblTextoCarroRota);
		
		JLabel lblTextoMotoRota = new JLabel("Moto:");
		lblTextoMotoRota.setBounds(699, 225, 48, 21);
		AbaVeiculos.add(lblTextoMotoRota);
		
		lblQtdCarretaRota = new JLabel("0");
		lblQtdCarretaRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdCarretaRota.setForeground(Color.RED);
		lblQtdCarretaRota.setBounds(491, 256, 42, 21);
		AbaVeiculos.add(lblQtdCarretaRota);
		
		lblQtdVanRota = new JLabel("0");
		lblQtdVanRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdVanRota.setForeground(Color.RED);
		lblQtdVanRota.setBounds(543, 257, 56, 21);
		AbaVeiculos.add(lblQtdVanRota);
		
		lblQtdCarroRota = new JLabel("0");
		lblQtdCarroRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdCarroRota.setForeground(Color.RED);
		lblQtdCarroRota.setBounds(628, 257, 32, 21);
		AbaVeiculos.add(lblQtdCarroRota);
		
		lblQtdMotoRota = new JLabel("0");
		lblQtdMotoRota.setHorizontalAlignment(SwingConstants.CENTER);
		lblQtdMotoRota.setForeground(Color.RED);
		lblQtdMotoRota.setBounds(689, 257, 42, 21);
		AbaVeiculos.add(lblQtdMotoRota);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(403, 14, 42, 286);
		AbaVeiculos.add(separator);
		
		JPanel AbaDadosELucros = new JPanel();
		tabbedPane.addTab("Dados/Lucro", null, AbaDadosELucros, null);
		AbaDadosELucros.setLayout(null);
		
		JLabel lblCarreta = new JLabel("Carreta:");
		lblCarreta.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCarreta.setBounds(10, 11, 76, 14);
		AbaDadosELucros.add(lblCarreta);
		
		JLabel lblCombustivelCarreta = new JLabel("-  Combust\u00EDvel:  Diesel");
		lblCombustivelCarreta.setBounds(20, 36, 148, 21);
		AbaDadosELucros.add(lblCombustivelCarreta);
		
		JLabel lblRendimentoCarreta = new JLabel("-  Rendimento:  8 Km/L");
		lblRendimentoCarreta.setBounds(20, 55, 148, 21);
		AbaDadosELucros.add(lblRendimentoCarreta);
		
		JLabel lblCargaMaximaCarreta = new JLabel("-  Carga M\u00E1xima:  30 toneladas");
		lblCargaMaximaCarreta.setBounds(20, 75, 184, 21);
		AbaDadosELucros.add(lblCargaMaximaCarreta);
		
		JLabel lblVelocidadeMediaCarreta = new JLabel("-  Velocidade M\u00E9dia:  60 Km/h");
		lblVelocidadeMediaCarreta.setBounds(20, 96, 184, 21);
		AbaDadosELucros.add(lblVelocidadeMediaCarreta);
		
		JLabel lblInfoCarreta = new JLabel("-  A cada Kg de carga, o rendimento \u00E9 reduzido em 0,0002 Km/L");
		lblInfoCarreta.setBounds(20, 117, 374, 21);
		AbaDadosELucros.add(lblInfoCarreta);
		
		JLabel lblVan = new JLabel("Van:");
		lblVan.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblVan.setBounds(10, 179, 48, 14);
		AbaDadosELucros.add(lblVan);
		
		JLabel lblCombustivelVan = new JLabel("-  Combust\u00EDvel:  Diesel");
		lblCombustivelVan.setBounds(20, 204, 148, 21);
		AbaDadosELucros.add(lblCombustivelVan);
		
		JLabel lblRendimentoVan = new JLabel("-  Rendimento:  10 Km/L");
		lblRendimentoVan.setBounds(20, 224, 148, 21);
		AbaDadosELucros.add(lblRendimentoVan);
		
		JLabel lblCargaMaximaVan = new JLabel("-  Carga M\u00E1xima:  3,5 toneladas");
		lblCargaMaximaVan.setBounds(20, 246, 184, 21);
		AbaDadosELucros.add(lblCargaMaximaVan);
		
		JLabel lblVelocidadeMediaVan = new JLabel("-  Velocidade M\u00E9dia:  80 Km/L");
		lblVelocidadeMediaVan.setBounds(20, 267, 184, 21);
		AbaDadosELucros.add(lblVelocidadeMediaVan);
		
		JLabel lblInfoVan = new JLabel("-  A cada Kg de carga, o rendimento \u00E9 reduzido em 0,001 Km/L");
		lblInfoVan.setBounds(20, 288, 374, 21);
		AbaDadosELucros.add(lblInfoVan);
		
		JLabel lblCarro = new JLabel("Carro:");
		lblCarro.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCarro.setBounds(10, 341, 48, 14);
		AbaDadosELucros.add(lblCarro);
		
		JLabel lblCombustivelCarro = new JLabel("-  Combust\u00EDvel:  Gasolina e \u00C1lcool");
		lblCombustivelCarro.setBounds(20, 366, 215, 21);
		AbaDadosELucros.add(lblCombustivelCarro);
		
		JLabel lblRendimentoCarro = new JLabel("-  Rendimento:  14 Km/L com gasolina, 12 Km/L com \u00E1lcool");
		lblRendimentoCarro.setBounds(20, 385, 351, 21);
		AbaDadosELucros.add(lblRendimentoCarro);
		
		JLabel lblCargaMaximaCarro = new JLabel("-  Carga M\u00E1xima:  360 Kg");
		lblCargaMaximaCarro.setBounds(20, 406, 184, 21);
		AbaDadosELucros.add(lblCargaMaximaCarro);
		
		JLabel lblVelocidadeMediaCarro = new JLabel("-  Velocidade M\u00E9dia:  100 Km/h");
		lblVelocidadeMediaCarro.setBounds(20, 427, 184, 21);
		AbaDadosELucros.add(lblVelocidadeMediaCarro);
		
		JLabel lblInfoCarro = new JLabel("-  A cada Kg de carga, o rendimento \u00E9 reduzido em 0,025 Km/L com gasolina e 0,0231 Km/L com \u00E1lcool");
		lblInfoCarro.setBounds(20, 448, 603, 21);
		AbaDadosELucros.add(lblInfoCarro);
		
		JLabel lblMoto = new JLabel("Moto:");
		lblMoto.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMoto.setBounds(10, 515, 48, 14);
		AbaDadosELucros.add(lblMoto);
		
		JLabel lblCombustivelMoto = new JLabel("-  Combust\u00EDvel:  Gasolina e \u00C1lcool");
		lblCombustivelMoto.setBounds(20, 540, 215, 21);
		AbaDadosELucros.add(lblCombustivelMoto);
		
		JLabel lblRendimentoMoto = new JLabel("-  Rendimento:  50 Km/L com gasolina, 43 Km/L com \u00E1lcool");
		lblRendimentoMoto.setBounds(20, 561, 351, 21);
		AbaDadosELucros.add(lblRendimentoMoto);
		
		JLabel lblCargaMaximaMoto = new JLabel("-  Carga M\u00E1xima:  3,5 toneladas");
		lblCargaMaximaMoto.setBounds(20, 580, 184, 21);
		AbaDadosELucros.add(lblCargaMaximaMoto);
		
		JLabel lblVelocidadeMediaMoto = new JLabel("-  Velocidade M\u00E9dia:  110 Km/h");
		lblVelocidadeMediaMoto.setBounds(20, 602, 184, 21);
		AbaDadosELucros.add(lblVelocidadeMediaMoto);
		
		JLabel lblInfoMoto = new JLabel("-  A cada Kg de carga, o rendimento \u00E9 reduzido em 0,3 Km/L com gasolina e 0,4 Km/L com \u00E1lcool");
		lblInfoMoto.setBounds(20, 621, 603, 21);
		AbaDadosELucros.add(lblInfoMoto);
		
		JLabel lblLucroEmpresa = new JLabel("Lucro da empresa (em porcentagem):");
		lblLucroEmpresa.setBounds(625, 12, 275, 14);
		AbaDadosELucros.add(lblLucroEmpresa);
		
		JLabel lblLucro = new JLabel("Lucro:");
		lblLucro.setBounds(635, 39, 48, 14);
		AbaDadosELucros.add(lblLucro);
		
		textFieldTaxaLucro = new JTextField();
		textFieldTaxaLucro.setText("20.0");
		textFieldTaxaLucro.setBounds(693, 36, 62, 20);
		AbaDadosELucros.add(textFieldTaxaLucro);
		textFieldTaxaLucro.setColumns(10);
		
		JButton btnAtualizarLucro = new JButton("Atualizar");
		btnAtualizarLucro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				
				if(comando.equals("Atualizar")) {
					double novaTaxaLucro = Double.parseDouble(textFieldTaxaLucro.getText());
					sistema.setLucro(novaTaxaLucro);
				}
			}
		});
		btnAtualizarLucro.setBounds(765, 35, 89, 23);
		AbaDadosELucros.add(btnAtualizarLucro);
		
		JLabel lblPrecoCombustveis = new JLabel("Pre\u00E7o dos combust\u00EDveis:");
		lblPrecoCombustveis.setBounds(625, 99, 176, 14);
		AbaDadosELucros.add(lblPrecoCombustveis);
		
		JLabel lblAlcool = new JLabel("\u00C1lcool:  R$");
		lblAlcool.setBounds(635, 124, 70, 14);
		AbaDadosELucros.add(lblAlcool);
		
		textFieldPrecoAlcool = new JTextField();
		textFieldPrecoAlcool.setText("3.499");
		textFieldPrecoAlcool.setBounds(736, 121, 96, 20);
		AbaDadosELucros.add(textFieldPrecoAlcool);
		textFieldPrecoAlcool.setColumns(10);
		
		JButton btnPrecoAlcool = new JButton("Atualizar");
		btnPrecoAlcool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				
				if(comando.equals("Atualizar")) {
					double novoPrecoAlcool = Double.parseDouble(textFieldPrecoAlcool.getText());
					sistema.setPrecoAlcool(novoPrecoAlcool);
				}
			}
		});
		btnPrecoAlcool.setBounds(842, 120, 89, 23);
		AbaDadosELucros.add(btnPrecoAlcool);
		
		JLabel lblGasolina = new JLabel("Gasolina:  R$");
		lblGasolina.setBounds(635, 160, 96, 14);
		AbaDadosELucros.add(lblGasolina);
		
		textFieldPrecoGasolina = new JTextField();
		textFieldPrecoGasolina.setText("4.449");
		textFieldPrecoGasolina.setColumns(10);
		textFieldPrecoGasolina.setBounds(736, 157, 96, 20);
		AbaDadosELucros.add(textFieldPrecoGasolina);
		
		JButton btnPrecoGasolina = new JButton("Atualizar");
		btnPrecoGasolina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				
				if(comando.equals("Atualizar")) {
					double novoPrecoGasolina = Double.parseDouble(textFieldPrecoGasolina.getText());
					sistema.setPrecoGasolina(novoPrecoGasolina);
				}
			}
		});
		btnPrecoGasolina.setBounds(842, 156, 89, 23);
		AbaDadosELucros.add(btnPrecoGasolina);
		
		JLabel lblDiesel = new JLabel("Diesel:  R$");
		lblDiesel.setBounds(635, 195, 70, 14);
		AbaDadosELucros.add(lblDiesel);
		
		textFieldPrecoDiesel = new JTextField();
		textFieldPrecoDiesel.setText("3.869");
		textFieldPrecoDiesel.setColumns(10);
		textFieldPrecoDiesel.setBounds(736, 192, 96, 20);
		AbaDadosELucros.add(textFieldPrecoDiesel);
		
		JButton btnPrecoDiesel = new JButton("Atualizar");
		btnPrecoDiesel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				
				if(comando.equals("Atualizar")) {
					double novoPrecoDiesel = Double.parseDouble(textFieldPrecoDiesel.getText());
					sistema.setPrecoDiesel(novoPrecoDiesel);
				}
			}
		});
		btnPrecoDiesel.setBounds(842, 191, 89, 23);
		AbaDadosELucros.add(btnPrecoDiesel);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(544, 11, 55, 234);
		AbaDadosELucros.add(separator_1);
		
		JPanel AbaEntregas = new JPanel();
		tabbedPane.addTab("Entregas", null, AbaEntregas, null);
		AbaEntregas.setLayout(null);
		
		JLabel lblOpcao_1 = new JLabel("1\u00AA Op\u00E7\u00E3o:  Ve\u00EDculo de menor custo");
		lblOpcao_1.setBounds(10, 234, 206, 22);
		AbaEntregas.add(lblOpcao_1);
		
		JLabel lblResultadoOpcao_1 = new JLabel("-");
		lblResultadoOpcao_1.setBounds(20, 286, 494, 22);
		AbaEntregas.add(lblResultadoOpcao_1);
		
		JLabel lblOpcao_2 = new JLabel("2\u00AA Op\u00E7\u00E3o:  Ve\u00EDculo mais r\u00E1pido");
		lblOpcao_2.setBounds(10, 361, 206, 22);
		AbaEntregas.add(lblOpcao_2);
		
		JLabel lblResultadoOpcao_2 = new JLabel("-");
		lblResultadoOpcao_2.setBounds(20, 419, 494, 22);
		AbaEntregas.add(lblResultadoOpcao_2);
		
		JLabel lblOpcao_3 = new JLabel("3\u00AA Op\u00E7\u00E3o:  Ve\u00EDculo com melhor custo-benef\u00EDcio");
		lblOpcao_3.setBounds(10, 488, 278, 14);
		AbaEntregas.add(lblOpcao_3);
		
		JLabel lblResultadoOpcao_3 = new JLabel("-");
		lblResultadoOpcao_3.setBounds(20, 540, 494, 22);
		AbaEntregas.add(lblResultadoOpcao_3);
		
		JLabel lblInsiraOsDados = new JLabel("Insira os dados da entrega para calcular as op\u00E7\u00F5es de transporte:");
		lblInsiraOsDados.setBounds(10, 11, 416, 22);
		AbaEntregas.add(lblInsiraOsDados);
		
		JLabel lblPesoDaCarga = new JLabel("Peso da carga (em Kg):");
		lblPesoDaCarga.setBounds(20, 44, 148, 22);
		AbaEntregas.add(lblPesoDaCarga);
		
		textFieldPesoDaCarga = new JTextField();
		textFieldPesoDaCarga.setText("0");
		textFieldPesoDaCarga.setBounds(264, 45, 96, 20);
		AbaEntregas.add(textFieldPesoDaCarga);
		textFieldPesoDaCarga.setColumns(10);
		
		JLabel lblDistancia = new JLabel("Dist\u00E2ncia (em Km):");
		lblDistancia.setBounds(20, 77, 148, 22);
		AbaEntregas.add(lblDistancia);
		
		textFieldDistancia = new JTextField();
		textFieldDistancia.setText("0");
		textFieldDistancia.setBounds(264, 78, 96, 20);
		AbaEntregas.add(textFieldDistancia);
		textFieldDistancia.setColumns(10);
		
		JLabel lblTempoMaximo = new JLabel("Tempo m\u00E1ximo para entrega (em horas):");
		lblTempoMaximo.setBounds(20, 110, 245, 22);
		AbaEntregas.add(lblTempoMaximo);
		
		textFieldTempoMaximo = new JTextField();
		textFieldTempoMaximo.setText("0");
		textFieldTempoMaximo.setBounds(264, 111, 96, 20);
		AbaEntregas.add(textFieldTempoMaximo);
		textFieldTempoMaximo.setColumns(10);
		
		JLabel lblResultadoConfirmar = new JLabel("-");
		lblResultadoConfirmar.setBounds(640, 467, 278, 22);
		AbaEntregas.add(lblResultadoConfirmar);
		
		JButton btnCalcularEntrega = new JButton("Calcular");
		btnCalcularEntrega.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				lblResultadoConfirmar.setText("-");
				Carreta carreta = new Carreta();
				Van van = new Van();
				Carro carro = new Carro();
				Moto moto = new Moto();
				double pesoDaCarga = Double.parseDouble(textFieldPesoDaCarga.getText());
				double distanciaParaEntrega = Double.parseDouble(textFieldDistancia.getText());
				double tempoMaximoParaEntrega = Double.parseDouble(textFieldTempoMaximo.getText());
				if(comando.equals("Calcular")) {
					
					// Variaveis locais para determinar o melhor veiculo em cada situa��o
					
					double tempo_menor_1 = 0.0;
					double tempo_menor_2 = 0.0;
					double menor_custo_1 = 0.0;
					double menor_custo_2 = 0.0;
					double melhor_custo_beneficio_1 = 0.0;
					double melhor_custo_beneficio_2 = 0.0;
					double custo_veiculo_mais_rapido_1 = 0.0;
					double custo_veiculo_mais_rapido_2 = 0.0;
					double tempo_veiculo_menor_custo_1 = 0.0;
					double tempo_veiculo_menor_custo_2 = 0.0;
					double tempo_veiculo_melhor_custo_beneficio_1 = 0.0;
					double tempo_veiculo_melhor_custo_beneficio_2 = 0.0;
					double menor_custo_carro = 0.0;
					double melhor_custo_beneficio_carro = 0.0;
					double menor_custo_moto = 0.0;
					double melhor_custo_beneficio_moto = 0.0;
					String veiculo_melhor_custo_beneficio_1 = "nenhum";
					String veiculo_melhor_custo_beneficio_2 = "nenhum";
					String veiculo_mais_rapido_1 = "nenhum";
					String veiculo_mais_rapido_2 = "nenhum";
					String veiculo_menor_custo_1 = "nenhum";
					String veiculo_menor_custo_2 = "nenhum";
					
					// Tempo maximo de cada veiculo
					
					double tempo_carreta = sistema.calculaTempoMaximo(distanciaParaEntrega, carreta.getVelocidadeMedia(), "carreta");
					double tempo_van = sistema.calculaTempoMaximo(distanciaParaEntrega, van.getVelocidadeMedia(), "van");
					double tempo_carro = sistema.calculaTempoMaximo(distanciaParaEntrega, carro.getVelocidadeMedia(), "carro");
					double tempo_moto = sistema.calculaTempoMaximo(distanciaParaEntrega, moto.getVelocidadeMedia(), "moto");
					
					// Custo de cada veiculo
					
					double custo_carreta = sistema.calculaCusto(distanciaParaEntrega, carreta.getRendimentoDiesel() - pesoDaCarga * carreta.getTaxaReducaoRendimentoDiesel(), "carreta", 1);
					double custo_van = sistema.calculaCusto(distanciaParaEntrega, van.getRendimentoDiesel() - pesoDaCarga * van.getTaxaReducaoRendimentoDiesel(), "van", 1);
					double custo_carro_gasolina = sistema.calculaCusto(distanciaParaEntrega, carro.getRendimentoGasolina() - pesoDaCarga * carro.getTaxaReducaoRendimentoGasolina(), "carro", 1);
					double custo_carro_alcool = sistema.calculaCusto(distanciaParaEntrega, carro.getRendimentoAlcool() - pesoDaCarga * carro.getTaxaReducaoRendimentoAlcool(), "carro", 2);
					double custo_carro = 0.0; // ser� atribuido a essa vari�vel o custo do carro com gasolina ou alcool
					double custo_moto_gasolina = sistema.calculaCusto(distanciaParaEntrega, moto.getRendimentoGasolina() - pesoDaCarga * moto.getTaxaReducaoRendimentoGasolina(), "moto", 1);
					double custo_moto_alcool = sistema.calculaCusto(distanciaParaEntrega, moto.getRendimentoAlcool() - pesoDaCarga * moto.getTaxaReducaoRendimentoAlcool(), "moto", 2);
					double custo_moto = 0.0; // ser� atribuido a essa vari�vel o custo da moto com gasolina ou alcool
					
					// Custo beneficio de cada veiculo
					
					double custo_beneficio_carreta = sistema.calculaCustoBeneficio(custo_carreta, tempo_carreta, "carreta");
					double custo_beneficio_van = sistema.calculaCustoBeneficio(custo_van, tempo_van, "van");
					double custo_beneficio_carro_gasolina = sistema.calculaCustoBeneficio(custo_carro_gasolina, tempo_carro, "carro");
					double custo_beneficio_carro_alcool = sistema.calculaCustoBeneficio(custo_carro_alcool, tempo_carro, "carro");
					double custo_beneficio_moto_gasolina = sistema.calculaCustoBeneficio(custo_moto_gasolina, tempo_moto, "moto");
					double custo_beneficio_moto_alcool = sistema.calculaCustoBeneficio(custo_moto_alcool, tempo_moto, "moto");
					
					// Verificar o veiculo mais rapido dentro dos crit�rios de peso da carga e tempo maximo
					
					if(tempoMaximoParaEntrega >= tempo_carreta && tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= carreta.getCargaMaxima() && pesoDaCarga <= van.getCargaMaxima()) {
						if(tempo_carreta < tempo_van) {
							tempo_menor_1 = tempo_carreta;
							veiculo_mais_rapido_1 = "carreta";
							custo_veiculo_mais_rapido_1 = custo_carreta;
						}else {
							tempo_menor_1 = tempo_van;
							veiculo_mais_rapido_1 = "van";
							custo_veiculo_mais_rapido_1 = custo_van;
						}
					}else if(tempoMaximoParaEntrega >= tempo_carreta && pesoDaCarga <= carreta.getCargaMaxima()) {
						tempo_menor_1 = tempo_carreta;
						veiculo_mais_rapido_1 = "carreta";
						custo_veiculo_mais_rapido_1 = custo_carreta;
					}else if(tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= van.getCargaMaxima()) {
						tempo_menor_1 = tempo_van;
						veiculo_mais_rapido_1 = "van";
						custo_veiculo_mais_rapido_1 = custo_van;
					}
					
					if(tempoMaximoParaEntrega >= tempo_carro && tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= carro.getCargaMaxima() && pesoDaCarga <= moto.getCargaMaxima()) {
						if(tempo_carro < tempo_moto) {
							tempo_menor_2 = tempo_carro;
							veiculo_mais_rapido_2 = "carro";
							custo_veiculo_mais_rapido_2 = custo_carro_gasolina;
						}else {
							tempo_menor_2 = tempo_moto;
							veiculo_mais_rapido_2 = "moto";
							custo_veiculo_mais_rapido_2 = custo_moto_gasolina;
						}
					}else if(tempoMaximoParaEntrega >= tempo_carro && pesoDaCarga <= carro.getCargaMaxima()) {
						tempo_menor_2 = tempo_carro;
						veiculo_mais_rapido_2 = "carro";
						custo_veiculo_mais_rapido_2 = custo_carro_gasolina;
					}else if(tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= moto.getCargaMaxima()) {
						tempo_menor_2 = tempo_moto;
						veiculo_mais_rapido_2 = "moto";
						custo_veiculo_mais_rapido_2 = custo_moto_gasolina;
					}
					
					if(tempo_menor_1 > 0.0 && tempo_menor_2 > 0.0) {
						if(tempo_menor_1 < tempo_menor_2) {
							sistema.setTempoMaximoVeiculoMaisRapido(tempo_menor_1);
							sistema.setTipoVeiculoMaisRapido(veiculo_mais_rapido_1);
							sistema.setValorFreteVeiculoMaisRapido(custo_veiculo_mais_rapido_1 + (custo_veiculo_mais_rapido_1 * sistema.getLucro() / 100));
						}else {
							sistema.setTempoMaximoVeiculoMaisRapido(tempo_menor_2);
							sistema.setTipoVeiculoMaisRapido(veiculo_mais_rapido_2);
							sistema.setValorFreteVeiculoMaisRapido(custo_veiculo_mais_rapido_2 + (custo_veiculo_mais_rapido_2 * sistema.getLucro() / 100));
						}
					}else if(tempo_menor_1 > 0.0) {
						sistema.setTempoMaximoVeiculoMaisRapido(tempo_menor_1);
						sistema.setTipoVeiculoMaisRapido(veiculo_mais_rapido_1);
						sistema.setValorFreteVeiculoMaisRapido(custo_veiculo_mais_rapido_1 + (custo_veiculo_mais_rapido_1 * sistema.getLucro() / 100));
					}else if(tempo_menor_2 > 0.0) {
						sistema.setTempoMaximoVeiculoMaisRapido(tempo_menor_2);
						sistema.setTipoVeiculoMaisRapido(veiculo_mais_rapido_2);
						sistema.setValorFreteVeiculoMaisRapido(custo_veiculo_mais_rapido_2 + (custo_veiculo_mais_rapido_2 * sistema.getLucro() / 100));
					}
					
					// Verificar o veiculo de menor custo dentro dos crit�rios de peso da carga e tempo maximo
					
					if(tempoMaximoParaEntrega >= tempo_carreta && tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= carreta.getCargaMaxima() && pesoDaCarga <= van.getCargaMaxima()) {
						if(custo_carreta < custo_van) {
							menor_custo_1 = custo_carreta;
							veiculo_menor_custo_1 = "carreta";
							tempo_veiculo_menor_custo_1 = tempo_carreta;
						}else {
							menor_custo_1 = custo_van;
							veiculo_menor_custo_1 = "van";
							tempo_veiculo_menor_custo_1 = tempo_van;
						}
					}else if(tempoMaximoParaEntrega >= tempo_carreta && pesoDaCarga <= carreta.getCargaMaxima()) {
						menor_custo_1 = custo_carreta;
						veiculo_menor_custo_1 = "carreta";
						tempo_veiculo_menor_custo_1 = tempo_carreta;
					}else if(tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= van.getCargaMaxima()) {
						menor_custo_1 = custo_van;
						veiculo_menor_custo_1 = "van";
						tempo_veiculo_menor_custo_1 = tempo_van;
					}
					
					if(custo_carro_gasolina < custo_carro_alcool) {
						menor_custo_carro = custo_carro_gasolina;
					}else {
						menor_custo_carro = custo_carro_alcool;
					}
					if(custo_moto_gasolina < custo_moto_alcool) {
						menor_custo_moto = custo_moto_gasolina;
					}else {
						menor_custo_moto = custo_moto_alcool;
					}
					
					if(tempoMaximoParaEntrega >= tempo_carro && tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= carro.getCargaMaxima() && pesoDaCarga <= moto.getCargaMaxima()) {
						if(menor_custo_carro < menor_custo_moto) {
							menor_custo_2 = menor_custo_carro;
							veiculo_menor_custo_2 = "carro";
							tempo_veiculo_menor_custo_2 = tempo_carro;
						}else {
							menor_custo_2 = menor_custo_moto;
							veiculo_menor_custo_2 = "moto";
							tempo_veiculo_menor_custo_2 = tempo_moto;
						}
					}else if(tempoMaximoParaEntrega >= tempo_carro && pesoDaCarga <= carro.getCargaMaxima()) {
						menor_custo_2 = menor_custo_carro;
						veiculo_menor_custo_2 = "carro";
						tempo_veiculo_menor_custo_2 = tempo_carro;
					}else if(tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= moto.getCargaMaxima()) {
						menor_custo_2 = menor_custo_moto;
						veiculo_menor_custo_2 = "moto";
						tempo_veiculo_menor_custo_2 = tempo_moto;
					}
					
					if(menor_custo_1 > 0.0 && menor_custo_2 > 0.0) {
						if(menor_custo_1 < menor_custo_2) {
							sistema.setTipoVeiculoMaisBarato(veiculo_menor_custo_1);
							sistema.setValorFreteVeiculoMaisBarato(menor_custo_1 + (menor_custo_1 * sistema.getLucro() / 100));
							sistema.setTempoMaximoVeiculoMaisBarato(tempo_veiculo_menor_custo_1);
						}else {
							sistema.setTipoVeiculoMaisBarato(veiculo_menor_custo_2);
							sistema.setValorFreteVeiculoMaisBarato(menor_custo_2 + (menor_custo_2 * sistema.getLucro() / 100));
							sistema.setTempoMaximoVeiculoMaisBarato(tempo_veiculo_menor_custo_2);
						}
					}else if(menor_custo_1 > 0.0) {
						sistema.setTipoVeiculoMaisBarato(veiculo_menor_custo_1);
						sistema.setValorFreteVeiculoMaisBarato(menor_custo_1 + (menor_custo_1 * sistema.getLucro() / 100));
						sistema.setTempoMaximoVeiculoMaisBarato(tempo_veiculo_menor_custo_1);
					}else if(menor_custo_2 > 0.0) {
						sistema.setTipoVeiculoMaisBarato(veiculo_menor_custo_2);
						sistema.setValorFreteVeiculoMaisBarato(menor_custo_2 + (menor_custo_2 * sistema.getLucro() / 100));
						sistema.setTempoMaximoVeiculoMaisBarato(tempo_veiculo_menor_custo_2);
					}
					
					// Verificar veiculo de melhor custo beneficio dentro dos crit�rios de peso da carga e tempo maximo
					
					if(tempoMaximoParaEntrega >= tempo_carreta && tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= carreta.getCargaMaxima() && pesoDaCarga <= van.getCargaMaxima()) {
						if(custo_beneficio_carreta > custo_beneficio_van) {
							melhor_custo_beneficio_1 = custo_carreta;
							tempo_veiculo_melhor_custo_beneficio_1 = tempo_carreta;
							veiculo_melhor_custo_beneficio_1 = "carreta";
						}else {
							melhor_custo_beneficio_1 = custo_van;
							tempo_veiculo_melhor_custo_beneficio_1 = tempo_van;
							veiculo_melhor_custo_beneficio_1 = "van";
						}
					}else if(tempoMaximoParaEntrega >= tempo_carreta && pesoDaCarga <= carreta.getCargaMaxima()) {
						melhor_custo_beneficio_1 = custo_carreta;
						tempo_veiculo_melhor_custo_beneficio_1 = tempo_carreta;
						veiculo_melhor_custo_beneficio_1 = "carreta";
					}else if(tempoMaximoParaEntrega >= tempo_van && pesoDaCarga <= van.getCargaMaxima()){
						melhor_custo_beneficio_1 = custo_van;
						tempo_veiculo_melhor_custo_beneficio_1 = tempo_van;
						veiculo_melhor_custo_beneficio_1 = "van";
					}
					
					if(custo_beneficio_carro_gasolina > custo_beneficio_carro_alcool) {
						melhor_custo_beneficio_carro = custo_beneficio_carro_gasolina;
					}else {
						melhor_custo_beneficio_carro = custo_beneficio_carro_alcool;
					}
					if(custo_beneficio_moto_gasolina > custo_beneficio_moto_alcool) {
						melhor_custo_beneficio_moto = custo_beneficio_moto_gasolina;
					}else {
						melhor_custo_beneficio_moto = custo_beneficio_moto_alcool;
					}
					
					if(tempoMaximoParaEntrega >= tempo_carro && tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= carro.getCargaMaxima() && pesoDaCarga <= moto.getCargaMaxima()) {
						if(melhor_custo_beneficio_carro > melhor_custo_beneficio_moto) {
							melhor_custo_beneficio_2 = custo_carro;
							tempo_veiculo_melhor_custo_beneficio_2 = tempo_carro;
							veiculo_melhor_custo_beneficio_2 = "carro";
						}else {
							melhor_custo_beneficio_2 = custo_moto;
							tempo_veiculo_melhor_custo_beneficio_2 = tempo_moto;
							veiculo_melhor_custo_beneficio_2 = "moto";
						}
					}else if(tempoMaximoParaEntrega >= tempo_carro && pesoDaCarga <= carro.getCargaMaxima()) {
						melhor_custo_beneficio_2 = custo_carro;
						tempo_veiculo_melhor_custo_beneficio_2 = tempo_carro;
						veiculo_melhor_custo_beneficio_2 = "carro";
					}else if(tempoMaximoParaEntrega >= tempo_moto && pesoDaCarga <= moto.getCargaMaxima()) {
						melhor_custo_beneficio_2 = custo_moto;
						tempo_veiculo_melhor_custo_beneficio_2 = tempo_moto;
						veiculo_melhor_custo_beneficio_2 = "moto";
					}
					
					if(melhor_custo_beneficio_1 > 0.0 && melhor_custo_beneficio_2 > 0.0) {
						if(melhor_custo_beneficio_1 > melhor_custo_beneficio_2) {
							sistema.setValorFreteVeiculoMelhorCustoBeneficio(melhor_custo_beneficio_1 + (melhor_custo_beneficio_1 * sistema.getLucro() / 100));
							sistema.setTempoMaximoVeiculoMelhorCustoBeneficio(tempo_veiculo_melhor_custo_beneficio_1);
							sistema.setTipoVeiculoMelhorCustoBeneficio(veiculo_melhor_custo_beneficio_1);
						}else {
							sistema.setValorFreteVeiculoMelhorCustoBeneficio(melhor_custo_beneficio_2 + (melhor_custo_beneficio_2 * sistema.getLucro() / 100));
							sistema.setTempoMaximoVeiculoMelhorCustoBeneficio(tempo_veiculo_melhor_custo_beneficio_2);
							sistema.setTipoVeiculoMelhorCustoBeneficio(veiculo_melhor_custo_beneficio_2);
						}
					}else if(melhor_custo_beneficio_1 > 0.0) {
						sistema.setValorFreteVeiculoMelhorCustoBeneficio(melhor_custo_beneficio_1 + (melhor_custo_beneficio_1 * sistema.getLucro() / 100));
						sistema.setTempoMaximoVeiculoMelhorCustoBeneficio(tempo_veiculo_melhor_custo_beneficio_1);
						sistema.setTipoVeiculoMelhorCustoBeneficio(veiculo_melhor_custo_beneficio_1);
					}else if(melhor_custo_beneficio_2 > 0.0) {
						sistema.setValorFreteVeiculoMelhorCustoBeneficio(melhor_custo_beneficio_2 + (melhor_custo_beneficio_2 * sistema.getLucro() / 100));
						sistema.setTempoMaximoVeiculoMelhorCustoBeneficio(tempo_veiculo_melhor_custo_beneficio_2);
						sistema.setTipoVeiculoMelhorCustoBeneficio(veiculo_melhor_custo_beneficio_2);
					}
					
					// Op��o 1: Ve�culo de menor custo
					
					if(sistema.getTempoMaximoVeiculoMaisBarato() != 0.0 && sistema.getTipoVeiculoMaisBarato() != "nenhum" && sistema.getValorFreteVeiculoMaisBarato() != 0.0) {
						sistema.setVeiculoMaisBarato(true);
						if(sistema.getTipoVeiculoMaisBarato() == "carreta" && sistema.getListaCarretaDisponivel().size() > 0) {
							lblResultadoOpcao_1.setText(sistema.getTipoVeiculoMaisBarato() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisBarato() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisBarato());
							lblResultadoOpcao_1.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisBarato() == "carreta" && sistema.getListaCarretaDisponivel().size() == 0) {
							lblResultadoOpcao_1.setText("O melhor ve�culo seria uma carreta, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_1.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisBarato() == "van" && sistema.getListaVanDisponivel().size() > 0) {
							lblResultadoOpcao_1.setText(sistema.getTipoVeiculoMaisBarato() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisBarato() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisBarato());
							lblResultadoOpcao_1.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisBarato() == "van" && sistema.getListaVanDisponivel().size() == 0) {
							lblResultadoOpcao_1.setText("O melhor ve�culo seria uma van, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_1.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisBarato() == "carro" && sistema.getListaCarroDisponivel().size() > 0) {
							lblResultadoOpcao_1.setText(sistema.getTipoVeiculoMaisBarato() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisBarato() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisBarato());
							lblResultadoOpcao_1.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisBarato() == "carro" && sistema.getListaCarroDisponivel().size() == 0) {
							lblResultadoOpcao_1.setText("O melhor ve�culo seria um carro, mas n�o h� nenhum dispon�vel!");
							lblResultadoOpcao_1.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisBarato() == "moto" && sistema.getListaMotoDisponivel().size() > 0) {
							lblResultadoOpcao_1.setText(sistema.getTipoVeiculoMaisBarato() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisBarato() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisBarato());
							lblResultadoOpcao_1.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisBarato() == "moto" && sistema.getListaMotoDisponivel().size() == 0) {
							lblResultadoOpcao_1.setText("O melhor ve�culo seria uma moto, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_1.setForeground(Color.RED);
						}
					}else {
						sistema.setVeiculoMaisBarato(false);
						lblResultadoOpcao_1.setText("N�o � poss�vel realizar essa entrega!");
						lblResultadoOpcao_1.setForeground(Color.RED);
					}
					
					// Op��o 2: Ve�culo mais r�pido
					
					if(sistema.getTempoMaximoVeiculoMaisRapido() != 0.0 && sistema.getTipoVeiculoMaisRapido() != "nenhum" && sistema.getValorFreteVeiculoMaisRapido() != 0.0) {
						sistema.setVeiculoMaisRapido(true);
						if(sistema.getTipoVeiculoMaisRapido() == "carreta" && sistema.getListaCarretaDisponivel().size() > 0) {
							lblResultadoOpcao_2.setText(sistema.getTipoVeiculoMaisRapido() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisRapido() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisRapido());
							lblResultadoOpcao_2.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisRapido() == "carreta" && sistema.getListaCarretaDisponivel().size() == 0) {
							lblResultadoOpcao_2.setText("O melhor ve�culo seria uma carreta, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_2.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisRapido() == "van" && sistema.getListaVanDisponivel().size() > 0) {
							lblResultadoOpcao_2.setText(sistema.getTipoVeiculoMaisRapido() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisRapido() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisRapido());
							lblResultadoOpcao_2.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisRapido() == "van" && sistema.getListaVanDisponivel().size() == 0) {
							lblResultadoOpcao_2.setText("O melhor ve�culo seria uma van, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_2.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisRapido() == "carro" && sistema.getListaCarroDisponivel().size() > 0) {
							lblResultadoOpcao_2.setText(sistema.getTipoVeiculoMaisRapido() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisRapido() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisRapido());
							lblResultadoOpcao_2.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisRapido() == "carro" && sistema.getListaCarroDisponivel().size() == 0) {
							lblResultadoOpcao_2.setText("O melhor ve�culo seria um carro, mas n�o h� nenhum dispon�vel!");
							lblResultadoOpcao_2.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMaisRapido() == "moto" && sistema.getListaMotoDisponivel().size() > 0) {
							lblResultadoOpcao_2.setText(sistema.getTipoVeiculoMaisRapido() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMaisRapido() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMaisRapido());
							lblResultadoOpcao_2.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMaisRapido() == "moto" && sistema.getListaMotoDisponivel().size() == 0) {
							lblResultadoOpcao_2.setText("O melhor ve�culo seria uma moto, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_2.setForeground(Color.RED);
						}
					}else {
						sistema.setVeiculoMaisRapido(false);
						lblResultadoOpcao_2.setText("N�o � poss�vel realizar essa entrega!");
						lblResultadoOpcao_2.setForeground(Color.RED);
					}
					
					// Op��o 3: Ve�culo de melhor custo-benef�cio
					
					if(sistema.getTempoMaximoVeiculoMelhorCustoBeneficio() != 0.0 && sistema.getTipoVeiculoMelhorCustoBeneficio() != "nenhum" && sistema.getValorFreteVeiculoMelhorCustoBeneficio() != 0.0) {
						sistema.setVeiculoMelhorCustoBeneficio(true);
						if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carreta" && sistema.getListaCarretaDisponivel().size() > 0) {
							lblResultadoOpcao_3.setText(sistema.getTipoVeiculoMelhorCustoBeneficio() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMelhorCustoBeneficio() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMelhorCustoBeneficio());
							lblResultadoOpcao_3.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carreta" && sistema.getListaCarretaDisponivel().size() == 0) {
							lblResultadoOpcao_3.setText("O melhor ve�culo seria uma carreta, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_3.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "van" && sistema.getListaVanDisponivel().size() > 0) {
							lblResultadoOpcao_3.setText(sistema.getTipoVeiculoMelhorCustoBeneficio() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMelhorCustoBeneficio() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMelhorCustoBeneficio());
							lblResultadoOpcao_3.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "van" && sistema.getListaVanDisponivel().size() == 0) {
							lblResultadoOpcao_3.setText("O melhor ve�culo seria uma van, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_3.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carro" && sistema.getListaCarroDisponivel().size() > 0) {
							lblResultadoOpcao_3.setText(sistema.getTipoVeiculoMelhorCustoBeneficio() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMelhorCustoBeneficio() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMelhorCustoBeneficio());
							lblResultadoOpcao_3.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carro" && sistema.getListaCarroDisponivel().size() == 0) {
							lblResultadoOpcao_3.setText("O melhor ve�culo seria um carro, mas n�o h� nenhum dispon�vel!");
							lblResultadoOpcao_3.setForeground(Color.RED);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "moto" && sistema.getListaMotoDisponivel().size() > 0) {
							lblResultadoOpcao_3.setText(sistema.getTipoVeiculoMelhorCustoBeneficio() + ", far� a entrega no m�ximo de " + sistema.getTempoMaximoVeiculoMelhorCustoBeneficio() + " horas, com um valor de frete de R$ " + sistema.getValorFreteVeiculoMelhorCustoBeneficio());
							lblResultadoOpcao_3.setForeground(Color.GREEN);
						}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "moto" && sistema.getListaMotoDisponivel().size() == 0) {
							lblResultadoOpcao_3.setText("O melhor ve�culo seria uma moto, mas n�o h� nenhuma dispon�vel!");
							lblResultadoOpcao_3.setForeground(Color.RED);
						}
					}else {
						sistema.setVeiculoMelhorCustoBeneficio(false);
						lblResultadoOpcao_3.setText("N�o � poss�vel realizar essa entrega!");
						lblResultadoOpcao_3.setForeground(Color.RED);
					}
				}
			}
		});
		btnCalcularEntrega.setBounds(264, 142, 96, 23);
		AbaEntregas.add(btnCalcularEntrega);
		
		JButton btnEscolherOpcao_1 = new JButton("Escolher");
		btnEscolherOpcao_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Escolher") && sistema.getVeiculoMaisBarato() == true) {
					if(sistema.getTipoVeiculoMaisBarato() == "carreta") {
						int i = sistema.getListaCarretaDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carreta carreta = new Carreta();
							carreta = (Carreta) sistema.getListaCarretaDisponivel().get(m);
							sistema.getListaCarretaEmRota().add(carreta);
							sistema.getListaCarretaDisponivel().remove(m);
						}
						int qtdCarretasRota = Integer.parseInt(lblQtdCarretaRota.getText()) + 1;
						lblQtdCarretaRota.setText(String.valueOf(qtdCarretasRota));
						int qtdCarretaDisponivel = Integer.parseInt(lblQtdCarretaDisponivel.getText()) - 1;
						lblQtdCarretaDisponivel.setText(String.valueOf(qtdCarretaDisponivel));
					}else if(sistema.getTipoVeiculoMaisBarato() == "van") {
						int i = sistema.getListaVanDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Van van = new Van();
							van = (Van) sistema.getListaVanDisponivel().get(m);
							sistema.getListaVanEmRota().add(van);
							sistema.getListaVanDisponivel().remove(m);
						}
						int qtdVansRota = Integer.parseInt(lblQtdVanRota.getText()) + 1;
						lblQtdVanRota.setText(String.valueOf(qtdVansRota));
						int qtdVanDisponivel = Integer.parseInt(lblQtdVanDisponivel.getText()) - 1;
						lblQtdVanDisponivel.setText(String.valueOf(qtdVanDisponivel));
					}else if(sistema.getTipoVeiculoMaisBarato() == "carro") {
						int i = sistema.getListaCarroDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carro carro = new Carro();
							carro = (Carro) sistema.getListaCarroDisponivel().get(m);
							sistema.getListaCarroEmRota().add(carro);
							sistema.getListaCarroDisponivel().remove(m);
						}
						int qtdCarrosRota = Integer.parseInt(lblQtdCarroRota.getText()) + 1;
						lblQtdCarroRota.setText(String.valueOf(qtdCarrosRota));
						int qtdCarroDisponivel = Integer.parseInt(String.valueOf(lblQtdCarroDisponivel.getText())) - 1;
						lblQtdCarroDisponivel.setText(String.valueOf(qtdCarroDisponivel));
					}else if(sistema.getTipoVeiculoMaisBarato() == "moto") {
						int i = sistema.getListaMotoDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Moto moto = new Moto();
							moto = (Moto) sistema.getListaMotoDisponivel().get(m);
							sistema.getListaMotoEmRota().add(moto);
							sistema.getListaMotoDisponivel().remove(m);
						}
						int qtdMotoRota = Integer.parseInt(lblQtdMotoRota.getText()) + 1;
						lblQtdMotoRota.setText(String.valueOf(qtdMotoRota));
						int qtdMotoDisponivel = Integer.parseInt(lblQtdMotoDisponivel.getText()) - 1;
						lblQtdMotoDisponivel.setText(String.valueOf(qtdMotoDisponivel));
					}
				}
			}
		});
		btnEscolherOpcao_1.setBounds(337, 234, 89, 23);
		AbaEntregas.add(btnEscolherOpcao_1);
		
		JButton btnEscolherOpcao_2 = new JButton("Escolher");
		btnEscolherOpcao_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Escolher") && sistema.getVeiculoMaisRapido() == true) {
					if(sistema.getTipoVeiculoMaisRapido() == "carreta") {
						int i = sistema.getListaCarretaDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carreta carreta = new Carreta();
							carreta = (Carreta) sistema.getListaCarretaDisponivel().get(m);
							sistema.getListaCarretaEmRota().add(carreta);
							sistema.getListaCarretaDisponivel().remove(m);
						}
						int qtdCarretasRota = Integer.parseInt(lblQtdCarretaRota.getText()) + 1;
						lblQtdCarretaRota.setText(String.valueOf(qtdCarretasRota));
						int qtdCarretaDisponivel = Integer.parseInt(lblQtdCarretaDisponivel.getText()) - 1;
						lblQtdCarretaDisponivel.setText(String.valueOf(qtdCarretaDisponivel));
					}else if(sistema.getTipoVeiculoMaisRapido() == "van") {
						int i = sistema.getListaVanDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Van van = new Van();
							van = (Van) sistema.getListaVanDisponivel().get(m);
							sistema.getListaVanEmRota().add(van);
							sistema.getListaVanDisponivel().remove(m);
						}
						int qtdVansRota = Integer.parseInt(lblQtdVanRota.getText()) + 1;
						lblQtdVanRota.setText(String.valueOf(qtdVansRota));
						int qtdVanDisponivel = Integer.parseInt(lblQtdVanDisponivel.getText()) - 1;
						lblQtdVanDisponivel.setText(String.valueOf(qtdVanDisponivel));
					}else if(sistema.getTipoVeiculoMaisRapido() == "carro") {
						int i = sistema.getListaCarroDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carro carro = new Carro();
							carro = (Carro) sistema.getListaCarroDisponivel().get(m);
							sistema.getListaCarroEmRota().add(carro);
							sistema.getListaCarroDisponivel().remove(m);
						}
						int qtdCarrosRota = Integer.parseInt(lblQtdCarroRota.getText()) + 1;
						lblQtdCarroRota.setText(String.valueOf(qtdCarrosRota));
						int qtdCarroDisponivel = Integer.parseInt(String.valueOf(lblQtdCarroDisponivel.getText())) - 1;
						lblQtdCarroDisponivel.setText(String.valueOf(qtdCarroDisponivel));
					}else if(sistema.getTipoVeiculoMaisRapido() == "moto") {
						int i = sistema.getListaMotoDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Moto moto = new Moto();
							moto = (Moto) sistema.getListaMotoDisponivel().get(m);
							sistema.getListaMotoEmRota().add(moto);
							sistema.getListaMotoDisponivel().remove(m);
						}
						int qtdMotoRota = Integer.parseInt(lblQtdMotoRota.getText()) + 1;
						lblQtdMotoRota.setText(String.valueOf(qtdMotoRota));
						int qtdMotoDisponivel = Integer.parseInt(lblQtdMotoDisponivel.getText()) - 1;
						lblQtdMotoDisponivel.setText(String.valueOf(qtdMotoDisponivel));
					}
				}
			}
		});
		btnEscolherOpcao_2.setBounds(337, 361, 89, 23);
		AbaEntregas.add(btnEscolherOpcao_2);
		
		JButton btnEscolherOpcao_3 = new JButton("Escolher");
		btnEscolherOpcao_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Escolher") && sistema.getVeiculoMelhorCustoBeneficio() == true) {
					if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carreta") {
						int i = sistema.getListaCarretaDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carreta carreta = new Carreta();
							carreta = (Carreta) sistema.getListaCarretaDisponivel().get(m);
							sistema.getListaCarretaEmRota().add(carreta);
							sistema.getListaCarretaDisponivel().remove(m);
						}
						int qtdCarretasRota = Integer.parseInt(lblQtdCarretaRota.getText()) + 1;
						lblQtdCarretaRota.setText(String.valueOf(qtdCarretasRota));
						int qtdCarretaDisponivel = Integer.parseInt(lblQtdCarretaDisponivel.getText()) - 1;
						lblQtdCarretaDisponivel.setText(String.valueOf(qtdCarretaDisponivel));
					}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "van") {
						int i = sistema.getListaVanDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Van van = new Van();
							van = (Van) sistema.getListaVanDisponivel().get(m);
							sistema.getListaVanEmRota().add(van);
							sistema.getListaVanDisponivel().remove(m);
						}
						int qtdVansRota = Integer.parseInt(lblQtdVanRota.getText()) + 1;
						lblQtdVanRota.setText(String.valueOf(qtdVansRota));
						int qtdVanDisponivel = Integer.parseInt(lblQtdVanDisponivel.getText()) - 1;
						lblQtdVanDisponivel.setText(String.valueOf(qtdVanDisponivel));
					}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "carro") {
						int i = sistema.getListaCarroDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Carro carro = new Carro();
							carro = (Carro) sistema.getListaCarroDisponivel().get(m);
							sistema.getListaCarroEmRota().add(carro);
							sistema.getListaCarroDisponivel().remove(m);
						}
						int qtdCarrosRota = Integer.parseInt(lblQtdCarroRota.getText()) + 1;
						lblQtdCarroRota.setText(String.valueOf(qtdCarrosRota));
						int qtdCarroDisponivel = Integer.parseInt(String.valueOf(lblQtdCarroDisponivel.getText())) - 1;
						lblQtdCarroDisponivel.setText(String.valueOf(qtdCarroDisponivel));
					}else if(sistema.getTipoVeiculoMelhorCustoBeneficio() == "moto") {
						int i = sistema.getListaMotoDisponivel().size() - 1;
						for(int m = i; m > i - 1; m--) {
							Moto moto = new Moto();
							moto = (Moto) sistema.getListaMotoDisponivel().get(m);
							sistema.getListaMotoEmRota().add(moto);
							sistema.getListaMotoDisponivel().remove(m);
						}
						int qtdMotoRota = Integer.parseInt(lblQtdMotoRota.getText()) + 1;
						lblQtdMotoRota.setText(String.valueOf(qtdMotoRota));
						int qtdMotoDisponivel = Integer.parseInt(lblQtdMotoDisponivel.getText()) - 1;
						lblQtdMotoDisponivel.setText(String.valueOf(qtdMotoDisponivel));
					}
				}
			}
		});
		btnEscolherOpcao_3.setBounds(337, 484, 89, 23);
		AbaEntregas.add(btnEscolherOpcao_3);
		
		JLabel lblTextoConfirmar_1 = new JLabel("Ap\u00F3s escolher uma op\u00E7\u00E3o ao lado, clique");
		lblTextoConfirmar_1.setBounds(640, 250, 320, 22);
		AbaEntregas.add(lblTextoConfirmar_1);
		
		JLabel lblTextoConfirmar_2 = new JLabel("no bot\u00E3o 'Confirmar' para concluir sua escolha.");
		lblTextoConfirmar_2.setBounds(640, 268, 278, 30);
		AbaEntregas.add(lblTextoConfirmar_2);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Confirmar")) {
					lblResultadoConfirmar.setText("Escolha efetuada com SUCESSO!");
					lblResultadoOpcao_1.setText("-");
					lblResultadoOpcao_2.setText("-");
					lblResultadoOpcao_3.setText("-");
					sistema.setTipoVeiculoMaisRapido("nenhum");
					sistema.setTempoMaximoVeiculoMaisRapido(0.0);
					sistema.setValorFreteVeiculoMaisRapido(0.0);
					sistema.setTipoVeiculoMaisBarato("nenhum");
					sistema.setTempoMaximoVeiculoMaisBarato(0.0);
					sistema.setValorFreteVeiculoMaisBarato(0.0);
					sistema.setTipoVeiculoMelhorCustoBeneficio("nenhum");
					sistema.setTempoMaximoVeiculoMelhorCustoBeneficio(0.0);
					sistema.setValorFreteVeiculoMelhorCustoBeneficio(0.0);
				}
			}
		});
		btnConfirmar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnConfirmar.setBounds(664, 339, 174, 67);
		AbaEntregas.add(btnConfirmar);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(586, 205, 44, 261);
		AbaEntregas.add(separator_2);
		
		JButton btnCarregarSistema = new JButton("Carregar Sistema");
		btnCarregarSistema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Carregar Sistema")) {
					Sistema sistemaRestaurado = new Sistema();
					RestaurarObjeto restaurar = new RestaurarObjeto();
					sistemaRestaurado = restaurar.restaurarSistema(sistema);
					sistema = sistemaRestaurado;
					int veiculosTotais = sistema.getListaCarretaDisponivel().size() + sistema.getListaCarretaEmRota().size() + sistema.getListaVanDisponivel().size() + sistema.getListaVanEmRota().size() +
											sistema.getListaCarroDisponivel().size() + sistema.getListaCarroEmRota().size() + sistema.getListaMotoDisponivel().size() + sistema.getListaMotoEmRota().size();
					lblQtdTotalVeiculos.setText(String.valueOf(veiculosTotais));
					lblQtdCarretaDisponivel.setText(String.valueOf(sistema.getListaCarretaDisponivel().size()));
					lblQtdCarretaRota.setText(String.valueOf(sistema.getListaCarretaEmRota().size()));
					lblQtdCarroDisponivel.setText(String.valueOf(sistema.getListaCarroDisponivel().size()));
					lblQtdCarroRota.setText(String.valueOf(sistema.getListaCarroEmRota().size()));
					lblQtdMotoDisponivel.setText(String.valueOf(sistema.getListaMotoDisponivel().size()));
					lblQtdMotoRota.setText(String.valueOf(sistema.getListaMotoEmRota().size()));
					lblQtdVanDisponivel.setText(String.valueOf(sistema.getListaVanDisponivel().size()));
					lblQtdVanRota.setText(String.valueOf(sistema.getListaVanEmRota().size()));
					textFieldTaxaLucro.setText(String.valueOf(sistema.getLucro()));
					textFieldPrecoAlcool.setText(String.valueOf(sistema.getPrecoAlcool()));
					textFieldPrecoDiesel.setText(String.valueOf(sistema.getPrecoDiesel()));
					textFieldPrecoGasolina.setText(String.valueOf(sistema.getPrecoGasolina()));
					
				}
			}
		});
		btnCarregarSistema.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnCarregarSistema.setBounds(118, 237, 297, 110);
		AbaInicio.add(btnCarregarSistema);
		
		JButton btnSalvarSistema = new JButton("Salvar Sistema");
		btnSalvarSistema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String comando = e.getActionCommand();
				if(comando.equals("Salvar Sistema")) {
					SalvarObjeto salvar = new SalvarObjeto();
					salvar.salvarSistema(sistema);
				}
			}
		});
		btnSalvarSistema.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnSalvarSistema.setBounds(483, 237, 297, 110);
		AbaInicio.add(btnSalvarSistema);
	}
}
